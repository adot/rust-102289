use std::marker::PhantomData;

pub(crate) trait Parser: Sized {
    type Output;
    fn parse(&mut self, _input: &str) -> Result<(), ()> {
        loop {}
    }
    fn map<F, B>(self, _f: F) -> Map<Self, F>
    where
        F: FnMut(Self::Output) -> B,
    {
        loop {}
    }
    fn expected<S>(self, _msg: S) -> Expected<Self, S> {
        loop {}
    }
}

pub(crate) struct Chainl1<P, Op>(P, Op);
impl<P, Op> Parser for Chainl1<P, Op>
where
    P: Parser,
    Op: Parser,
    Op::Output: FnOnce(P::Output, P::Output) -> P::Output,
{
    type Output = P::Output;
}
pub(crate) fn chainl1<P, Op>(_parser: P, _op: Op) -> Chainl1<P, Op>
where
    P: Parser,
    Op: Parser,
    Op::Output: FnOnce(P::Output, P::Output) -> P::Output,
{
    loop {}
}

pub(crate) struct Map<P, F>(P, F);
impl<A, B, P, F> Parser for Map<P, F>
where
    P: Parser<Output = A>,
    F: FnMut(A) -> B,
{
    type Output = B;
}
pub(crate) struct Expected<P, S>(P, S);
impl<P, S> Parser for Expected<P, S>
where
    P: Parser,
{
    type Output = P::Output;
}

pub(crate) struct Token;
impl Parser for Token {
    type Output = ();
}

pub(crate) struct TokensCmp<C, T> {
    _marker: PhantomData<(C, T)>,
}
impl<C, T> Parser for TokensCmp<C, T> {
    type Output = T;
}
pub(crate) fn tokens_cmp<C, T>(_tokens: T, _cmp: C) -> TokensCmp<C, T>
where
    C: FnMut(T::Item, ()) -> bool,
    T: IntoIterator,
{
    loop {}
}

pub(crate) fn char(_c: char) -> Token {
    loop {}
}

pub(crate) fn string<'a>(s: &'static str) -> impl Parser<Output = &'a str> {
    string_cmp(s, |_l, _r| true)
}

pub(crate) fn string_cmp<'a, C>(s: &'static str, cmp: C) -> impl Parser<Output = &'a str>
where
    C: FnMut(char, ()) -> bool,
{
    tokens_cmp(s.chars(), cmp).map(move |_| s).expected(s)
}

pub fn chainl1_error_consume() {
    fn first<T, U>(t: T, _: U) -> T {
        t
    }
    let mut p = chainl1(string("abc"), char(',').map(|_| first));
    assert!(p.parse("abc,ab").is_err());
}
